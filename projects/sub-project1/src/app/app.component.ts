import { Component,  ViewEncapsulation, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { environment } from './../environments/environment';
import { Router } from '@angular/router';

@Component({
  // selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class AppComponent implements OnInit {
  @Input('state')
  set state(state: string) {
      console.log('client-b received state', state);
  }

  @Output() message = new EventEmitter<any>();
  constructor(
    private router: Router) {
  }

  ngOnInit() {
    this.router.initialNavigation(); // Manually triggering initial navigation for @angular/elements ?
    // Standalone mode
    if (environment.standalone) {
      this.router.navigate(['/client-b/page1']);
    }
    // just for demonstration!
    setTimeout(() => {
      this.message.next('client b initialized!');
    }, 2000);
}
}
