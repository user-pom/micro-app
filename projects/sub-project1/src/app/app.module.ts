import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Injector } from '@angular/core';
import {createCustomElement} from '@angular/elements';
import {RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { WildCardComponent } from './wild-card/wild-card.component';
import { CoreComponent } from './core/core.component'; 

@NgModule({
  declarations: [
    AppComponent,
    Page1Component,
    Page2Component,
    WildCardComponent,
    CoreComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: 'client-b', component: CoreComponent, children: [
        { path: 'page1', component: Page1Component },
        { path: 'page2', component: Page2Component },
      ]},
      {path: '**', component: WildCardComponent}], { useHash: true }),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  entryComponents: [AppComponent],
  bootstrap: []
})
export class AppModule {
  constructor(private injector: Injector) {
  }

  ngDoBootstrap() {
    const appElement = createCustomElement(AppComponent, { injector: this.injector});
    customElements.define('client-b', appElement);

  }
 }
