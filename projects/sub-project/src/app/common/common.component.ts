import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';

@Component({
  // selector: 'app-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class CommonComponent implements OnInit {
  control = new FormControl();
  value$: Observable<string>;
  constructor() { }

  ngOnInit() {
    this.control.valueChanges.subscribe(x => console.log(x));
    this.value$ = this.control.valueChanges;
  }
  clickMe(): void {
    console.log('ouch!');
  }

}
