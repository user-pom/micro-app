import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { WildCardComponent } from './wild-card/wild-card.component';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { CommonComponent } from './common/common.component';
import { CoreComponent } from './core/core.component';
import { PushPipe } from './push.pipe';
import { ReactiveFormsModule, FormControl } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    WildCardComponent,
    Page1Component,
    Page2Component,
    CommonComponent,
    CoreComponent,
    PushPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: 'client-a', component: CoreComponent, children: [
        { path: 'page1', component: Page1Component },
        { path: 'page2', component: Page2Component },
      ]},
      {path: '**', component: WildCardComponent}], { useHash: true }),
      ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [
    AppComponent,
    CommonComponent
  ]
})
export class AppModule {

  constructor(private injector: Injector) {
  }

  ngDoBootstrap() {
    const appElement = createCustomElement(AppComponent, { injector: this.injector});
    customElements.define('client-a', appElement);

    const widgetElement = createCustomElement(CommonComponent, { injector: this.injector});
    customElements.define('client-a-widget', widgetElement);
  }
 }
