import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
 
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {
  control = new FormControl();
  constructor() { }

  ngOnInit() {
  }

}
