import { Component, OnInit } from '@angular/core';
import {StateService} from './../state.service';

@Component({
  selector: 'app-sidebar-cmp',
  templateUrl: './sidebar-cmp.component.html',
  styleUrls: ['./sidebar-cmp.component.css']
})
export class SidebarCmpComponent implements OnInit {

  constructor(private stateService: StateService) { }

  ngOnInit() {
  }

  sendState() {
    this.stateService.setState('Info from Shell');
  }
}
