import { Component} from '@angular/core';
import { StateService } from './state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'micro-demo';
  constructor(private stateService: StateService) {
  }

  config = {
    'sub-project': {
      loaded: false,
      path: 'sub-project/main.js',
      element: 'client-a'
    },
    'sub-project1': {
      loaded: false,
      path: 'sub-project1/main.js',
      element: 'client-b'
    },
  };

  ngOnInit() {
    this.load('sub-project');
    this.load('sub-project1');
  }

  load(name: string): void {

    const configItem = this.config[name];
    if (configItem.loaded) return;

    const content = document.getElementById('content');



    const script = document.createElement('script');
    script.src = configItem.path;
    content.appendChild(script);

    const element: HTMLElement = document.createElement(configItem.element);
    content.appendChild(element);




    element.addEventListener('message', msg => this.handleMessage(msg));
    element.setAttribute('state', 'init');

    script.onerror = () => console.error(`error loading ${configItem.path}`);


    this.stateService.registerClient(element);

  }

  handleMessage(msg): void {
    console.log('shell received message: ', msg.detail);
  }
}
