import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private sidebarVisible = false;

  constructor() { }
  ngOnInit() {
  }


  sidebarToggle(){
      const body = document.getElementsByTagName('body')[0];

      if(!this.sidebarVisible) {
          body.classList.add('nav-open');
          this.sidebarVisible = true;
      } else {
          this.sidebarVisible = false;
          body.classList.remove('nav-open');
      }
  }

}
